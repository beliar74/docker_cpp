git clone https://github.com/llvm/llvm-project.git -b llvmorg-10.0.0

cd llvm_project && mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Release -G Ninja -DCMAKE_INSTAL_PREFIX=/usr \
	-DLLVM_ENABLE_PROJECTS="clang;compiler-rt;libc;libclc;libcxx;libcxxabi;libunwind" \
	-DLLVM_TARGETS_TO_BUILD="X86" .. \
	&& ninja && ninja install