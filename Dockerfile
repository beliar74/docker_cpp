FROM archlinux:latest

ADD . /code
WORKDIR /code

RUN "yes" | pacman -Syyuu --noconfirm  && "yes" | pacman -S cmake ninja base-devel git --noconfirm && bash build_llvm.sh